package ru.syskaev.edu.geekbrains.java.core1.lesson07;

/**
 *  Course: Java Core 1
 *  Homework for lesson 7
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 02, 2018
 */
public class Cat {

    private static final int DEFAULT_APPETITE = 15;

    final private String name;
    final private int appetite;
    private boolean hungry = true;

    public String getName() {
        return name;
    }

    public int getAppetite() {
        return appetite;
    }

    public boolean isHungry() {
        return hungry;
    }

    public Cat(String name) {
        this.name = name;
        appetite = DEFAULT_APPETITE;
    }

    public Cat(String name, int appetite) {
        this.name = name;
        this.appetite = appetite;
    }

    public void eatFrom(Plate plate) {
        if(!hungry)
            return;
        if(plate.eat(appetite))
            hungry = false;
    }

    public void reset() {
        hungry = true;
    }

}
