package ru.syskaev.edu.geekbrains.java.core1.lesson07;

import ru.syskaev.edu.geekbrains.java.core1.lesson07.UI.CatPanel;
import ru.syskaev.edu.geekbrains.java.core1.lesson07.UI.FeedingPanel;
import ru.syskaev.edu.geekbrains.java.core1.lesson07.UI.MainWindow;

/**
 *  Course: Java Core 1
 *  Homework for lesson 7
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 02, 2018
 */
public class FeedingController {

    private static final int DEFAULT_CATS_AMOUNT = 4;
    private static final String[] DEFAULT_CAT_NAMES = {"Barsik", "Marus'ka", "Tishka", "Vas'ka"};

    final private Plate plate = new Plate();
    final private Cat[] cats = new Cat[DEFAULT_CATS_AMOUNT];
    final private MainWindow mainWindow;
    private FeedingPanel feedingPanel;

    public Cat[] getCats() {
        return cats;
    }

    public Plate getPlate() {
        return plate;
    }

    public void setFeedingPanel(FeedingPanel feedingPanel) {
        this.feedingPanel = feedingPanel;
    }

    public FeedingController() {
        for(int i = 0; i != DEFAULT_CATS_AMOUNT; i++)
            cats[i] = new Cat(DEFAULT_CAT_NAMES[i], 10 + i * 5);
        mainWindow = new MainWindow(this);
        checkFood();
    }

    public void startFeeding() {
        mainWindow.setVisible(true);
    }

    public void appendLog(String newLogLine) {
        feedingPanel.appendLog(newLogLine);
    }

    public void checkFood() {
        int availableFood = plate.getFoodAmount();
        appendLog("Food amount = " + plate.getFoodAmount());
        if(availableFood == 0)
            appendLog("Plate is empty");
        else if (availableFood == Plate.getCapacity())
            appendLog("Plate is full");
        feedingPanel.repaint();
    }

    public void reset() {
        for(Cat cat : cats)
            cat.reset();
        for(CatPanel catPanel : mainWindow.getCatPanels())
            catPanel.updateHungryLabel();
        plate.reset();
        feedingPanel.repaint();
        feedingPanel.clearLog();
        checkFood();
    }

}
