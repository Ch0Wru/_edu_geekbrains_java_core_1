package ru.syskaev.edu.geekbrains.java.core1.lesson07.UI;

import ru.syskaev.edu.geekbrains.java.core1.lesson07.Cat;
import ru.syskaev.edu.geekbrains.java.core1.lesson07.FeedingController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

import static ru.syskaev.edu.geekbrains.java.core1.lesson07.UI.UIConsts.*;

/**
 *  Course: Java Core 1
 *  Homework for lesson 7
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 02, 2018
 */
public class CatPanel extends JPanel{

    final private int numberOfCatPanels;
    final private int numberAtPanel;
    final private FeedingController feedingController;
    final private Cat cat;

    final private int rightPanelDefaultWidth;
    final private JPanel rightPanel;
    private JTextField hungryLabel;

    public CatPanel(int numberOfCatPanels, int numberAtPanel, FeedingController feedingController, Cat cat) {
        this.numberOfCatPanels = numberOfCatPanels;
        this.numberAtPanel = numberAtPanel;
        this.feedingController = feedingController;
        this.cat = cat;

        rightPanelDefaultWidth = (int)(DEFAULT_RIGHT_PANEL_WIDTH_RELATIVE_TO_CAT_PANEL
                * DEFAULT_BOTTOM_PANEL_SIZE.width / numberOfCatPanels);
        rightPanel = getNewRightPanel();

        setBackground(Color.CYAN);
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 2));

        add(new JPanelImage("./res/cat" + numberAtPanel + ".jpg"), BorderLayout.CENTER);
        add(rightPanel, BorderLayout.EAST);
    }

    private JPanel getNewRightPanel() {
        JPanel tempRightPanel = new JPanel();
        tempRightPanel.setPreferredSize(new Dimension(rightPanelDefaultWidth, Integer.MAX_VALUE));
        tempRightPanel.setLayout(new BoxLayout(tempRightPanel, BoxLayout.Y_AXIS));
        tempRightPanel.setBackground(RIGHT_PANEL_COLOR);

        tempRightPanel.add(getDefaultNameLabel());
        tempRightPanel.add(getDefaultAppetiteLabel());
        hungryLabel = getDefaultHungryLabel();
        updateHungryLabel();
        tempRightPanel.add(hungryLabel);
        tempRightPanel.add(getNewFeedingButton());

        return tempRightPanel;
    }

    private JTextField getDefaultNameLabel() {
        return getNewLabel(cat.getName());
    }

    private JTextField getDefaultAppetiteLabel() {
        return getNewLabel("\u2615 = " + Integer.toString(cat.getAppetite()));
    }

    private JTextField getDefaultHungryLabel() {
        return getNewLabel("");
    }

    private JTextField getNewLabel(String text) {
        JTextField tempTextField = new JTextField(text);
        tempTextField.setEditable(false);
        tempTextField.setHorizontalAlignment(JTextField.CENTER);
        tempTextField.setBackground(RIGHT_PANEL_COLOR);
        tempTextField.setForeground(Color.BLACK);
        return tempTextField;
    }

    public void updateHungryLabel() {
        if(cat.isHungry()) {
            hungryLabel.setText("Hungry");
            hungryLabel.setForeground(Color.RED);
        }
        else {
            hungryLabel.setText("Satisfied");
            hungryLabel.setForeground(SATISFIED_COLOR);
        }
    }

    private JPanel getNewFeedingButton() {
        JButton tempButton = new JButton();
        tempButton.setMargin(new Insets(0, 0, 0, 0));
        tempButton.setText("Feed!");
        tempButton.addActionListener(e -> feedCat());

        JPanel tempPanel = new JPanel();
        tempPanel.setLayout(new BorderLayout());
        tempPanel.add(tempButton, BorderLayout.CENTER);

        return tempPanel;
    }

    private void feedCat() {
        if(cat.isHungry()) {
            cat.eatFrom(feedingController.getPlate());
            if(cat.isHungry())
                feedingController.appendLog("Not enough food");
            else {
                feedingController.appendLog(cat.getName() + " ate " + cat.getAppetite() + " units of food");
                feedingController.checkFood();
            }
            updateHungryLabel();
        }
        else
            feedingController.appendLog(cat.getName() + " is not hungry");
    }

    public class JPanelImage extends JPanel {

        private Image backgroundImage;

        public JPanelImage(String fileName) {
            try {
                backgroundImage = ImageIO.read(new File(fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(
                    backgroundImage,
                    0,0,
                    DEFAULT_BOTTOM_PANEL_SIZE.width / numberOfCatPanels - rightPanelDefaultWidth,
                    DEFAULT_BOTTOM_PANEL_SIZE.height,
                    this);
        }

    }

}
