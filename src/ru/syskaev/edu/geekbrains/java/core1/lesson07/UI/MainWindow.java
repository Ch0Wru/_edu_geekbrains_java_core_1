package ru.syskaev.edu.geekbrains.java.core1.lesson07.UI;

import ru.syskaev.edu.geekbrains.java.core1.lesson07.Cat;
import ru.syskaev.edu.geekbrains.java.core1.lesson07.FeedingController;

import javax.swing.*;
import java.awt.*;

import static ru.syskaev.edu.geekbrains.java.core1.lesson07.UI.UIConsts.*;

/**
 *  Course: Java Core 1
 *  Homework for lesson 7
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 02, 2018
 */
public class MainWindow extends JFrame {

    final private FeedingController feedingController;
    final private FeedingPanel feedingPanel;
    final private JPanel bottomPanel;
    private CatPanel[] catPanels;

    public CatPanel[] getCatPanels() {
        return catPanels;
    }

    public MainWindow(FeedingController feedingController) {
        this.feedingController = feedingController;
        feedingPanel = new FeedingPanel(feedingController);
        feedingController.setFeedingPanel(feedingPanel);
        bottomPanel = getNewBottomPanel();

        setTitle("Feeding");
        setResizable(false);
        setBounds(getDefaultWindowBounds());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        add(feedingPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
        setNewCatPanels();
    }

    private Rectangle getDefaultWindowBounds() {
        Rectangle bounds = new Rectangle();
        bounds.width = DEFAULT_WINDOW_SIZE.width;
        bounds.height = DEFAULT_WINDOW_SIZE.height;
        bounds.x = (SCREEN_SIZE.width - DEFAULT_WINDOW_SIZE.width) / 2;
        bounds.y = (SCREEN_SIZE.height - DEFAULT_WINDOW_SIZE.height) / 2;
        return bounds;
    }

    private JPanel getNewBottomPanel() {
        JPanel tempBottomPanel = new JPanel();
        tempBottomPanel.setPreferredSize(new Dimension(DEFAULT_BOTTOM_PANEL_SIZE.width, DEFAULT_BOTTOM_PANEL_SIZE.height));
        tempBottomPanel.setLayout(new BoxLayout(tempBottomPanel, BoxLayout.X_AXIS));
        return tempBottomPanel;
    }

    private void setNewCatPanels() {
        Cat[] cats = feedingController.getCats();
        int catsAmount = cats.length;
        catPanels = new CatPanel[catsAmount];

        for(int i = 0; i != catsAmount; i++) {
            catPanels[i] = new CatPanel(catsAmount, i, feedingController, cats[i]);
            bottomPanel.add(catPanels[i]);
        }
    }

}
