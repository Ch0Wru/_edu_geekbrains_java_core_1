package ru.syskaev.edu.geekbrains.java.core1.lesson07.UI;

import ru.syskaev.edu.geekbrains.java.core1.lesson07.FeedingController;

import javax.swing.*;
import java.awt.*;

import static ru.syskaev.edu.geekbrains.java.core1.lesson07.UI.UIConsts.*;

/**
 *  Course: Java Core 1
 *  Homework for lesson 7
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 02, 2018
 */
public class FeedingPanel extends JPanel {

    final private FeedingController feedingController;
    private JTextPane logArea;
    private StringBuilder logText = new StringBuilder("Feeding...\n");

    public FeedingPanel(FeedingController feedingController) {
        this.feedingController = feedingController;

        setBackground(FEEDING_PANEL_COLOR);
        setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 2));
        setLayout(new BorderLayout());

        add(getNewLogPanel(), BorderLayout.EAST);
    }

    private JPanel getNewLogPanel() {
        JPanel tempLogPanel = new JPanel();
        tempLogPanel.setPreferredSize(new Dimension(DEFAULT_LOG_PANEL_WIDTH, Integer.MAX_VALUE));
        tempLogPanel.setLayout(new BorderLayout());
        tempLogPanel.add(getNewLogScrollPanel(), BorderLayout.CENTER);
        tempLogPanel.add(getNewButtonsPanel(), BorderLayout.SOUTH);
        return tempLogPanel;
    }

    private JScrollPane getNewLogScrollPanel() {
        JScrollPane tempLogScrollPanel = new JScrollPane(getNewLogArea());
        tempLogScrollPanel.setPreferredSize(new Dimension(DEFAULT_LOG_PANEL_WIDTH, Integer.MAX_VALUE));
        tempLogScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        tempLogScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        return tempLogScrollPanel;
    }

    private JTextPane getNewLogArea() {
        JTextPane tempLogArea = new JTextPane();
        tempLogArea.setEditable(false);
        tempLogArea.setBackground(Color.BLACK);
        tempLogArea.setForeground(Color.WHITE);
        tempLogArea.setText(logText.toString());
        logArea = tempLogArea;
        return tempLogArea;
    }

    private JPanel getNewButtonsPanel() {
        JPanel tempPanel = new JPanel();
        tempPanel.setPreferredSize(new Dimension(DEFAULT_LOG_PANEL_WIDTH, DEFAULT_FEEDING_BUTTON_HEIGHT));
        tempPanel.setLayout(new BoxLayout(tempPanel, BoxLayout.X_AXIS));
        tempPanel.add(getNewAddFoodButton());
        tempPanel.add(getNewResetButton());
        return tempPanel;
    }

    private JButton getNewAddFoodButton() {
        JButton tempButton = new JButton();
        tempButton.setMaximumSize(new Dimension(DEFAULT_LOG_PANEL_WIDTH / 2, DEFAULT_FEEDING_BUTTON_HEIGHT));
        tempButton.setSelected(false);
        tempButton.setText("Add food");
        tempButton.addActionListener(e -> addFoodToPlate());
        return tempButton;
    }

    private void addFoodToPlate() {
        feedingController.getPlate().addFood();
        feedingController.checkFood();
    }

    private JButton getNewResetButton() {
        JButton tempButton = new JButton();
        tempButton.setMaximumSize(new Dimension(DEFAULT_LOG_PANEL_WIDTH / 2, DEFAULT_FEEDING_BUTTON_HEIGHT));
        tempButton.setSelected(false);
        tempButton.setText("Reset");
        tempButton.addActionListener(e -> reset());
        return tempButton;
    }

    private void reset() {
        feedingController.reset();
    }

    public void appendLog(String newLogLine) {
        logText.append(newLogLine);
        logText.append("\n");
        logArea.setText(logText.toString());
    }

    public void clearLog() {
        logText = new StringBuilder("Feeding...\n");
        logArea.setText(logText.toString());
    }

    static class plateImage {
        public static final double SCALE_FACTOR_X = SCREEN_SIZE.width / 1920.0;
        public static final double SCALE_FACTOR_Y = SCREEN_SIZE.height / 1080.0;
        public static final int LU_POINT_X = (int)(100 * SCALE_FACTOR_X);
        public static final int LU_POINT_Y = (int)(100 * SCALE_FACTOR_X);
        public static final int WIDTH = (int)(475 * SCALE_FACTOR_X);
        public static final int HEIGHT = (int)(200 * SCALE_FACTOR_Y);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.DARK_GRAY);
        g.drawArc(plateImage.LU_POINT_X, plateImage.LU_POINT_Y, plateImage.WIDTH, plateImage.HEIGHT,0, 360);
        for(int delta = 0; delta != 90; delta += 3) {
            int scaleDelta = (int)(delta * plateImage.SCALE_FACTOR_Y);
            g.drawArc(plateImage.LU_POINT_X, plateImage.LU_POINT_Y - scaleDelta, plateImage.WIDTH,
                    plateImage.HEIGHT + 2 * scaleDelta,
                    180, 180);
        }
        g.setColor(PLATE_BACKGROUND_COLOR);
        g.fillArc(plateImage.LU_POINT_X, plateImage.LU_POINT_Y, plateImage.WIDTH, plateImage.HEIGHT,0, 360);
        g.setColor(FOOD_COLOR);
        g.fillArc(plateImage.LU_POINT_X, plateImage.LU_POINT_Y,
                plateImage.WIDTH, plateImage.HEIGHT,0,
                (int)(feedingController.getPlate().getProportion() * 360));
    }

}
