package ru.syskaev.edu.geekbrains.java.core1.lesson07;

/**
 *  Course: Java Core 1
 *  Homework for lesson 7
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 02, 2018
 */
public class Plate {

    private static final int CAPACITY = 100;
    private static final int DEFAULT_ADDITIVE = 10;

    private int foodAmount;

    public int getFoodAmount() {
        return foodAmount;
    }

    public static int getCapacity() {
        return CAPACITY;
    }

    public Plate() {
        foodAmount = 0;
    }

    public Plate(int foodAmount) {
        this.foodAmount = foodAmount;
    }

    public boolean eat(int food) {
        if(foodAmount >= food) {
            foodAmount -= food;
            return true;
        }
        return false;
    }

    public void addFood() {
        addFood(DEFAULT_ADDITIVE);
    }

    public void addFood(int food) {
        foodAmount += food;
        if(foodAmount > CAPACITY)
            foodAmount = CAPACITY;
    }

    public double getProportion() {
        return (double)foodAmount / CAPACITY;
    }

    public void reset() {
        foodAmount = 0;
    }

}
