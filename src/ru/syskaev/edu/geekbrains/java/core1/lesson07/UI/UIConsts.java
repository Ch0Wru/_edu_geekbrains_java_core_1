package ru.syskaev.edu.geekbrains.java.core1.lesson07.UI;

import java.awt.*;

/**
 *  Course: Java Core 1
 *  Homework for lesson 7
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 02, 2018
 */
class UIConsts {

    static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();

    static final double DEFAULT_WINDOW_WIDTH_RELATIVE_TO_SCREEN_WIDTH = 0.5;
    static final double DEFAULT_WINDOW_HEIGHT_RELATIVE_TO_SCREEN_HEIGHT = 0.6;
    static final Dimension DEFAULT_WINDOW_SIZE = new Dimension(
            (int)(SCREEN_SIZE.width * DEFAULT_WINDOW_WIDTH_RELATIVE_TO_SCREEN_WIDTH),
            (int)(SCREEN_SIZE.height * DEFAULT_WINDOW_HEIGHT_RELATIVE_TO_SCREEN_HEIGHT));

    static final double DEFAULT_BOTTOM_PANEL_HEIGHT_RELATIVE_TO_WINDOW_HEIGHT = 0.25;
    static final Dimension DEFAULT_BOTTOM_PANEL_SIZE = new Dimension(
            (int)(DEFAULT_WINDOW_SIZE.width),
            (int)(DEFAULT_BOTTOM_PANEL_HEIGHT_RELATIVE_TO_WINDOW_HEIGHT * DEFAULT_WINDOW_SIZE.height));

    static final double DEFAULT_RIGHT_PANEL_WIDTH_RELATIVE_TO_CAT_PANEL = 0.3;

    static final double DEFAULT_LOG_PANEL_WIDTH_RELATIVE_TO_FEEDING_PANEL = 0.25;
    static final int DEFAULT_LOG_PANEL_WIDTH =
            (int)(DEFAULT_LOG_PANEL_WIDTH_RELATIVE_TO_FEEDING_PANEL * DEFAULT_WINDOW_SIZE.width);

    static final int DEFAULT_FEEDING_BUTTON_HEIGHT = 45;

    static final Color FEEDING_PANEL_COLOR = new Color(230, 230, 250); // Lavender
    static final Color RIGHT_PANEL_COLOR = new Color(222, 184, 135); // BurlyWood
    static final Color SATISFIED_COLOR = new Color(0, 128, 0); // Green
    static final Color PLATE_BACKGROUND_COLOR = new Color(255, 248, 220); // Cornsilk
    static final Color FOOD_COLOR = new Color(205, 92, 92); // IndianRed

}
