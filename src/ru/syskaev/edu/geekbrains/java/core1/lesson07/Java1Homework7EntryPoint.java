package ru.syskaev.edu.geekbrains.java.core1.lesson07;

/**
 *  Course: Java Core 1
 *  Homework for lesson 7
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 02, 2018
 */
public class Java1Homework7EntryPoint {

    public static void main(String args[]) {
        FeedingController feedingController = new FeedingController();
        feedingController.startFeeding();
    }

}
