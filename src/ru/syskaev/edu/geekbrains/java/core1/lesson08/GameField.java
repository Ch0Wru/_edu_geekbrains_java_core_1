package ru.syskaev.edu.geekbrains.java.core1.lesson08;

import java.awt.*;
import java.util.Random;

import static ru.syskaev.edu.geekbrains.java.core1.lesson08.Consts.*;

/**
 *  Course: Java Core 1
 *  Homework for lesson 8
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 04, 2018
 */
public class GameField {

    private final Random random = new Random();
    private final GameFieldCell[][] gameFieldCells =
            new GameFieldCell[GAME_FIELD_HORIZONTAL_CELLS_NUMBER][GAME_FIELD_VERTICAL_CELLS_NUMBER];
    private final Snake snake;

    public GameField() {
        for(int i = 0; i != GAME_FIELD_HORIZONTAL_CELLS_NUMBER; i++)
            for(int j = 0; j != GAME_FIELD_VERTICAL_CELLS_NUMBER; j++)
                gameFieldCells[i][j] = new GameFieldCell(i, j);
        GameFieldCell cell =
                gameFieldCells[GAME_FIELD_HORIZONTAL_CELLS_NUMBER / 2][GAME_FIELD_VERTICAL_CELLS_NUMBER / 2];
        snake = new Snake(this, cell);
    }

    synchronized public Snake getSnake() { return snake; }

    public GameFieldCell[][] getGameFieldCells() {
        return gameFieldCells;
    }

    public GameFieldCell getCellAt(int numX, int numY) {
        return gameFieldCells[numX][numY];
    }

    public void createCellWithType(CellType cellType) {
        while(true) {
            int i = random.nextInt(GAME_FIELD_HORIZONTAL_CELLS_NUMBER);
            int j = random.nextInt(GAME_FIELD_VERTICAL_CELLS_NUMBER);
            if(gameFieldCells[i][j].getCellType() != CellType.EMPTY)
                continue;
            gameFieldCells[i][j].setCellType(cellType);
            break;
        }
    }

    class GameFieldCell {

        private CellType cellType = CellType.EMPTY;
        private int numX, numY;
        private int coordX, coordY;
        private Image img = null;

        public GameFieldCell(int i, int j) {
            numX = i; numY = j;
            coordX = i * CELL_LINEAR_SIZE;
            coordY = j * CELL_LINEAR_SIZE;
        }

        public int getNumX() { return numX; }

        public int getNumY() { return numY; }

        public int getCoordX() { return coordX; }

        public int getCoordY() { return coordY; }

        public Image getImg() { return img; }

        public CellType getCellType() { return cellType; }

        public void setCellType(CellType cellType) {
            this.cellType = cellType;
            switch (cellType) {
                case ROCK:
                    img = ROCK_IMG;
                    break;
                case FOOD:
                    if (Math.random() < 1.0 / 2)
                        img = APPLE_IMG;
                    else
                        img = CHERRY_IMG;
            }
        }

    }

}
