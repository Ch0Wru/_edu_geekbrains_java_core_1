package ru.syskaev.edu.geekbrains.java.core1.lesson08;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import static ru.syskaev.edu.geekbrains.java.core1.lesson08.Consts.*;

/**
 *  Course: Java Core 1
 *  Homework for lesson 8
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 04, 2018
 */
public class GameWindow extends JFrame {

    private final GameField gameField;
    private final PaintPanel paintPanel;

    public GameWindow(GameField gameField) {
        this.gameField = gameField;

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(GAME_FIELD_WIDTH, GAME_FIELD_HEIGHT);
        setLocationRelativeTo(null);
        setResizable(false);
        setUndecorated(true);

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent key) {
                handleKey(key.getKeyCode());
            }
        });

        paintPanel = new PaintPanel();
        add(paintPanel);
    }

    synchronized public void handleKey(int keyCode) {
        if(keyCode == KeyEvent.VK_ESCAPE)
            System.exit(0);
        else
            gameField.getSnake().setNewDirection(keyCode);
    }

    @Override
    public void repaint() {
        super.repaint();
        paintPanel.repaint();
    }

    class PaintPanel extends JPanel {

        @Override
        public void paint (Graphics graphics){
            super.paint(graphics);
            graphics.setColor(FIELD_COLOR);
            graphics.fillRect(0,0, GAME_FIELD_WIDTH, GAME_FIELD_HEIGHT);
            paintGrid(graphics);
            paintCells(graphics);
        }

        private void paintGrid (Graphics graphics){
            graphics.setColor(Color.GRAY);
            for (int i = 0; i < GAME_FIELD_WIDTH; i += CELL_LINEAR_SIZE)
                graphics.drawLine(i, 0, i, GAME_FIELD_HEIGHT);
            for (int i = 0; i < GAME_FIELD_HEIGHT; i += CELL_LINEAR_SIZE)
                graphics.drawLine(0, i, GAME_FIELD_WIDTH, i);
        }

        private void paintCells(Graphics graphics){
            for (GameField.GameFieldCell[] cells : gameField.getGameFieldCells())
                for (GameField.GameFieldCell cell : cells) {
                    switch (cell.getCellType()) {
                        case EMPTY:
                            break;
                        case SEGMENT:
                            graphics.setColor(Color.BLUE);
                            graphics.fillOval(cell.getCoordX(), cell.getCoordY(), CELL_LINEAR_SIZE, CELL_LINEAR_SIZE);
                            break;
                        default:
                            paintImg(graphics, cell);
                    }
                }
        }

        private void paintImg(Graphics graphics, GameField.GameFieldCell cell) {
            graphics.drawImage(cell.getImg(), cell.getCoordX(), cell.getCoordY(),
                    CELL_LINEAR_SIZE, CELL_LINEAR_SIZE, this);
        }

    }

}
