package ru.syskaev.edu.geekbrains.java.core1.lesson08;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 *  Course: Java Core 1
 *  Homework for lesson 8
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 04, 2018
 */
class Consts {

    static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();

    static final int CELL_LINEAR_SIZE = 40;
    static final int GAME_FIELD_HORIZONTAL_CELLS_NUMBER = (int)(SCREEN_SIZE.width / CELL_LINEAR_SIZE / 2);
    static final int GAME_FIELD_VERTICAL_CELLS_NUMBER = (int)(SCREEN_SIZE.height / CELL_LINEAR_SIZE / 3 * 2);
    static final int GAME_FIELD_WIDTH = GAME_FIELD_HORIZONTAL_CELLS_NUMBER * CELL_LINEAR_SIZE + 1;
    static final int GAME_FIELD_HEIGHT = GAME_FIELD_VERTICAL_CELLS_NUMBER * CELL_LINEAR_SIZE + 1;

    enum CellType { EMPTY, FOOD, ROCK, SEGMENT }
    enum Direction { UP, DOWN, RIGHT, LEFT}

    static final Color FIELD_COLOR = new Color(152, 251, 152); // PaleGreen

    static Image APPLE_IMG, CHERRY_IMG, ROCK_IMG;

    static void calcConsts() {
        try {
            APPLE_IMG = ImageIO.read(new File("./res/apple.png"));
            CHERRY_IMG = ImageIO.read(new File("./res/cherry.png"));
            ROCK_IMG = ImageIO.read(new File("./res/rock.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
