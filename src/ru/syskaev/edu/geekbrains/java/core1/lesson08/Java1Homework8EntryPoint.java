package ru.syskaev.edu.geekbrains.java.core1.lesson08;

/**
 *  Course: Java Core 1
 *  Homework for lesson 8
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 04, 2018
 */
public class Java1Homework8EntryPoint {

    public static void main(String args[]) {
        Consts.calcConsts();
        Game game = new Game();
        game.start();
    }

}
