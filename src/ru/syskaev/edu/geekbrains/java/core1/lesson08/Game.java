package ru.syskaev.edu.geekbrains.java.core1.lesson08;

import javax.swing.*;

/**
 *  Course: Java Core 1
 *  Homework for lesson 8
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 04, 2018
 */
public class Game {

    private static final int ITERATION_DURATION = 300;
    private static final int ROCK_FREQUENCY = 20; // Iterations
    private static final int FOOD_FREQUENCY = 8; // Iterations

    private final GameField gameField;
    private final GameWindow gameWindow;
    private int iterationCount = -1;

    public Game() {
        gameField = new GameField();
        gameWindow = new GameWindow(gameField);
    }

    public void start() {
        gameWindow.setVisible(true);
        while(true) {
            try { Thread.sleep(ITERATION_DURATION); } catch (InterruptedException e) { e.printStackTrace(); }
            if(doIteration() == false)
                break;
        }
        new GameOverWindow().setVisible(true);
    }

    private boolean doIteration() {
        if(iterationCount % ROCK_FREQUENCY == 0)
            gameField.createCellWithType(Consts.CellType.ROCK);
        if(iterationCount % FOOD_FREQUENCY == 0)
            gameField.createCellWithType(Consts.CellType.FOOD);
        boolean iterationResult = gameField.getSnake().doIteration();
        if(iterationResult == false)
            return false;
        gameWindow.repaint();
        iterationCount++;
        return true;
    }

    class GameOverWindow extends JFrame {

        public GameOverWindow() {
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setSize(250, 120);
            setLocationRelativeTo(null);
            setResizable(false);
            add(getNewTextField());
        }

        private JTextField getNewTextField() {
            JTextField tempTextField = new JTextField();
            tempTextField.setText("GameOver!   Your result: " + gameField.getSnake().getScore());
            tempTextField.setEditable(false);
            tempTextField.setHorizontalAlignment(SwingConstants.CENTER);
            return tempTextField;
        }

    }

}
