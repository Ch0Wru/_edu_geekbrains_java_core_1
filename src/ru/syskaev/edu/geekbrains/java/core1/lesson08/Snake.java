package ru.syskaev.edu.geekbrains.java.core1.lesson08;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import static ru.syskaev.edu.geekbrains.java.core1.lesson08.Consts.*;

/**
 *  Course: Java Core 1
 *  Homework for lesson 8
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Aug 04, 2018
 */
public class Snake {

    final private List<GameField.GameFieldCell> segments = new ArrayList<>(0);
    final private GameField gameField;
    private GameField.GameFieldCell head, tail;
    private Direction direction = Direction.UP;

    public Snake(GameField gameField, GameField.GameFieldCell cell) {
        this.gameField = gameField;
        segments.add(cell);
        cell.setCellType(CellType.SEGMENT);
        head = cell;
        tail = cell;
    }

    public int getScore() {return segments.size();}

    public boolean doIteration() {
        int nextCellNumX = getNextHeadPosition()[0];
        int nextCellNumY = getNextHeadPosition()[1];
        if(!checkNumX(nextCellNumX) || !checkNumY(nextCellNumY))
            return false;
        GameField.GameFieldCell nextCell = gameField.getCellAt(nextCellNumX, nextCellNumY);
        if(nextCell.getCellType() == CellType.ROCK || nextCell.getCellType() == CellType.SEGMENT)
            return false;
        if(nextCell.getCellType() == CellType.FOOD)
            segments.add(segments.size(), segments.get(segments.size() - 1));

        move();

        segments.set(0, nextCell);
        nextCell.setCellType(CellType.SEGMENT);
        head = nextCell;

        tail.setCellType(CellType.EMPTY);
        tail = segments.get(segments.size() - 1);

        return true;
    }

    public void move() {
        int length = segments.size();
        for(int i = length - 1; i > 0; i--) {
            segments.set(i, segments.get(i - 1));
        }
    }

    private boolean checkNumX(int numX) {return numX >= 0 && numX < GAME_FIELD_HORIZONTAL_CELLS_NUMBER;}

    private boolean checkNumY(int numY) {return numY >= 0 && numY < GAME_FIELD_VERTICAL_CELLS_NUMBER;}


    public int[] getNextHeadPosition() {
        switch(direction) {
            case UP:
                return new int[] {head.getNumX(), head.getNumY() - 1};
            case DOWN:
                return new int[] {head.getNumX(), head.getNumY() + 1};
            case RIGHT:
                return new int[] {head.getNumX() + 1, head.getNumY()};
            case LEFT:
                return new int[] {head.getNumX() - 1, head.getNumY()};
            default:
                return null;
        }
    }

    synchronized public void setNewDirection(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_UP:
                direction = Direction.UP;
                break;
            case KeyEvent.VK_DOWN:
                direction = Direction.DOWN;
                break;
            case KeyEvent.VK_RIGHT:
                direction = Direction.RIGHT;
                break;
            case KeyEvent.VK_LEFT:
                direction = Direction.LEFT;
        }
    }

}
