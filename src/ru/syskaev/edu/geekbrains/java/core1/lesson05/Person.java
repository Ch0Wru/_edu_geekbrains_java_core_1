package ru.syskaev.edu.geekbrains.java.core1.lesson05;

import java.net.URI;
import java.net.URISyntaxException;

/**
 *  Course: Java Core 1
 *  Homework for lesson 5
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 24, 2018
 */
public class Person {

    private String name;
    private String occupation;
    private URI email;
    private long phoneNumber;
    private long salary;
    private int age;

    public Person(String name, String occupation, String email, String phoneNumber, long salary, int age) {
        this.name = name;
        this.occupation = occupation;
        this.salary = salary;
        this.age = age;
        this.phoneNumber = Long.parseLong(phoneNumber.replaceAll("[^0-9]", ""));
        try {
            this.email = new URI(email);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        String string = "";
        string += "Name:\t" + name + "\n";
        string += "Occup.:\t" + occupation + "\n";
        string += "E-mail:\t" + email.toString() + "\n";
        string += "Tel.:\t+" + phoneNumber + "\n";
        string += "Salary:\t" + salary + " \u20BD\n";
        string += "Age:\t" + age;
        return string;
    }

    public void printToConsole() {
        System.out.println(this.toString());
    }

}
