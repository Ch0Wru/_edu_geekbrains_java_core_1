package ru.syskaev.edu.geekbrains.java.core1.lesson05;

/**
 *  Course: Java Core 1
 *  Homework for lesson 5
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 24, 2018
 */
public class Java1Homework5EntryPoint {

    private static final int MINIMAL_AGE = 40;

    public static void main(String[] args) {
        Person[] persons = createTestArray();
        for(Person person : persons)
            if(person.getAge() > MINIMAL_AGE) {
                person.printToConsole();
                System.out.println();
            }
    }

    private static Person[] createTestArray() {
        Person[] persons = new Person[5];
        persons[0] = new Person("Ivanov Ivan Ivanovich", "Junior Programmer",
                "iii@qmail.cc", "+7(777)777-77-71", 100000, 20);
        persons[1] = new Person("Petrov Petr Petrovich", "Middle Programmer",
                "ppp@qmail.cc", "+7(777)777-77-72", 200000, 30);
        persons[2] = new Person("Sidorov Sidor Sidorovich", "Senior Programmer",
                "sssi@qmail.cc", "+7(777)777-77-73", 300000, 40);
        persons[3] = new Person("Sergeev Sergey Sergeevich", "Team Lead",
                "ssse@qmail.cc", "+7(777)777-77-74", 400000, 50);
        persons[4] = new Person("Alexeev Alexey Alexseevich", "Technical Director",
                "aaa@qmail.cc", "+7(777)777-77-75", 500000, 60);
        return persons;
    }

}
