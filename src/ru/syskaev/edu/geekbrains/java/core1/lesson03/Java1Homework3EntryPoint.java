package ru.syskaev.edu.geekbrains.java.core1.lesson03;

import java.util.Scanner;

/**
 *  Course: Java Core 1
 *  Homework for lesson 3
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 16, 2018
 */
public class Java1Homework3EntryPoint {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("Choose your game (1 - \"guess the number\" / 2 - \"guess the word\") ...");
            String usersInput = scanner.next();
            if(usersInput.length() == 1 && ( (usersInput.charAt(0)) == '1' || (usersInput.charAt(0)) == '2') ) {
                if(usersInput.charAt(0) == '1')
                    GuessTheNumberGame.startTheGame();
                else
                    GuessTheWordGame.startTheGame();
                break;
            }
            else
                System.out.println("Incorrect input");
        }
    }

}
