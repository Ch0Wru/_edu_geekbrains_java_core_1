package ru.syskaev.edu.geekbrains.java.core1.lesson03;

import java.util.Random;
import java.util.Scanner;

/**
 *  Course: Java Core 1
 *  Homework for lesson 3
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 16, 2018
 */
public class GuessTheWordGame {

    private static final String[] WORDS = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli",
            "carrot", "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive", "pea",
            "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};

    private static final int MAX_ACTUAL_LENGTH_OF_WORD = 15;
    private static String DEFAULT_HINT_TEMPLATE = "###############";

    static private String enigmaticWord;

    public static void startTheGame() {
        enigmaticWord = createEnigmaticWord();
        System.out.println("I made a random word, try to guess... ");
        checkUsersAttempt();
    }

    private static void checkUsersAttempt() {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            String usersInput = scanner.next();
            if(usersInput.length() <= MAX_ACTUAL_LENGTH_OF_WORD) {
                if (usersInput.equals(enigmaticWord)) {
                    System.out.println("You win!");
                    break;
                }
                else {
                    StringBuilder hint = new StringBuilder(DEFAULT_HINT_TEMPLATE);
                    for(int i = 0; i < enigmaticWord.length() && i < usersInput.length(); i++)
                        if(enigmaticWord.charAt(i) == usersInput.charAt(i))
                            hint.setCharAt(i, enigmaticWord.charAt(i));
                    System.out.println("You did not guess right\nHint for you: " + hint);
                }
            } else
                System.out.println("Your word is excessively long\nTry again...");
        }
    }

    private static String createEnigmaticWord() {
        int tempRandomInt = (new Random()).nextInt(WORDS.length);
        return WORDS[tempRandomInt];
    }

}
