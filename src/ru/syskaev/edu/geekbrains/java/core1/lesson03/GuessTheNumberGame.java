package ru.syskaev.edu.geekbrains.java.core1.lesson03;

import java.util.Random;
import java.util.Scanner;

/**
 *  Course: Java Core 1
 *  Homework for lesson 3
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 16, 2018
 */
public class GuessTheNumberGame {

    private static final int MAX_POSSIBLE_NUMBER = 9;
    private static final int DEFAULT_NUMBER_OF_ATTEMPTS = 3;

    static private int enigmaticNumber;
    static private int attemptCount;

    public static void startTheGame() {
        enigmaticNumber = createEnigmaticNumber();
        attemptCount = 0;
        System.out.println("I made a random number between 0 and 9 inclusive, you have "
                + DEFAULT_NUMBER_OF_ATTEMPTS + " attempts, try to guess... ");
        checkUsersAttempt();
    }

    private static void checkUsersAttempt() {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            String usersInput = scanner.next();
            if(usersInput.length() == 1 && ( (usersInput.charAt(0)) >= '0' || (usersInput.charAt(0)) <= '9') ) {
                attemptCount++;
                int attempt = Integer.parseInt(usersInput);
                if(attempt == enigmaticNumber)
                    reportVictory();
                else if(attemptCount >= DEFAULT_NUMBER_OF_ATTEMPTS)
                    reportLose();
                else {
                    if(attempt > enigmaticNumber)
                        System.out.println("Your number is bigger than mine, try again...");
                    else
                        System.out.println("Your number is smaller than mine, try again...");
                    checkUsersAttempt();
                }
                break;
            } else
                System.out.println("Incorrect input\nTry again...");
        }
    }

    private static int createEnigmaticNumber() {
        return (new Random()).nextInt(MAX_POSSIBLE_NUMBER + 1);
    }

    private static void reportVictory() {
        System.out.println("You win!");
        checkRepeatTheGame();
    }

    private static void reportLose() {
        System.out.println("You lose, I'm sorry :(");
        checkRepeatTheGame();
    }

    private static void checkRepeatTheGame() {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("Wanna repeat? (1 - yes / 0 - no) ...");
            String usersInput = scanner.next();
            if(usersInput.length() == 1 && ( (usersInput.charAt(0)) == '0' || (usersInput.charAt(0)) == '1') ) {
                if(usersInput.charAt(0) == '1')
                    GuessTheNumberGame.startTheGame();
                break;
            }
            else
                System.out.println("Incorrect input");
        }
    }

}
