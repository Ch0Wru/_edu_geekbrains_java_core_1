package ru.syskaev.edu.geekbrains.java.core1.lesson04;

/**
 *  Course: Java Core 1
 *  Homework for lesson 4
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 21, 2018
 */
public class Java1Homework4EntryPoint {

    private static final int MAX_LINEAR_SIZE_OF_GRID = 15;

    public static void main(String[] args) {
        System.out.format("Select the linear size of the grid (3...%d) ...\n", MAX_LINEAR_SIZE_OF_GRID);
        int size = Utils.inputIntBetween(3, MAX_LINEAR_SIZE_OF_GRID);
        TicTakToeGame game = new TicTakToeGame(size);
        game.startGame();
    }

}
