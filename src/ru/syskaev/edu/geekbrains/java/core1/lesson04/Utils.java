package ru.syskaev.edu.geekbrains.java.core1.lesson04;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *  Course: Java Core 1
 *  Homework for lesson 4
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 21, 2018
 */
public class Utils {

    private static Scanner scanner;

    public static int inputIntBetween(int minInt, int maxInt) {
        scanner = new Scanner(System.in);
        while(true) {
            try {
                int tempInt = scanner.nextInt();
                if (tempInt < minInt || tempInt > maxInt)
                    System.out.println("Number out of range, please try again ...");
                else
                    return tempInt;
            } catch (InputMismatchException e) {
                System.out.println("Incorrect input, please try again ...");
                scanner.next();
            }
        }
    }

    public static int[] inputPair(int minInt, int maxInt) {
        scanner = new Scanner(System.in);
        while(true) {
            try {
                String tempStr = scanner.nextLine();
                int coordX = Integer.parseInt(tempStr.split(" ")[0]);
                int coordY = Integer.parseInt(tempStr.split(" ")[1]);
                if (coordX < minInt || coordX > maxInt)
                    System.out.println("First number out of range, please try again ...");
                else if (coordY < minInt || coordY > maxInt)
                    System.out.println("Second number out of range, please try again ...");
                else
                    return (new int[]{coordX, coordY});
            } catch (InputMismatchException | NumberFormatException | ArrayIndexOutOfBoundsException e) {
                System.out.println("Incorrect input, please try again ...");
            }
        }
    }

}
