package ru.syskaev.edu.geekbrains.java.core1.lesson04;

import java.util.Random;

/**
 *  Course: Java Core 1
 *  Homework for lesson 4
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 21, 2018
 */
public class TicTakToeGame {

    private static final char MARK_X = 'X';
    private static final char MARK_O = 'O';
    private static final char EMPTY = '.';

    private final int gridLinearSize;
    private final int victoryCondition;
    private final char grid[][];
    private final Random random = new Random();

    public TicTakToeGame(int gridLinearSize) {
        this.gridLinearSize = gridLinearSize;
        victoryCondition = (gridLinearSize + 1) / 2 + 1;
        grid = new char[gridLinearSize][gridLinearSize];
        fillGrid();
    }

    private void fillGrid() {
        for (int i = 0; i != gridLinearSize; i++)
            for (int j = 0; j != gridLinearSize; j++)
                grid[i][j] = EMPTY;
    }

    private void printGrid() {
        System.out.println("\nActual game status:");
        for(int i = 0; i != gridLinearSize; i++) {
            for (int j = 0; j != gridLinearSize; j++)
                System.out.print(grid[i][j]);
            System.out.println();
        }
    }

    public void startGame() {
        System.out.format("\nGame format: %d x %d\nCondition of victory: %d your marks at line\n",
                gridLinearSize, gridLinearSize, victoryCondition);
        System.out.println("Axis: X \u2191 , Y \u2192");
        printGrid();
        boolean tempBoolean;
        do tempBoolean = doTurn(); while (!tempBoolean);
    }

    //true - game over, false - game continues
    private boolean doTurn() {
        requestUserTurn();
        printGrid();
        if(checkGameOver())
            return true;
        doResponseTurn();
        printGrid();
        if(checkGameOver())
            return true;
        return false;
    }

    private void requestUserTurn() {
        System.out.println("\nYour turn (format: \"X Y\") ...");
        while(true) {
            int pair[] = Utils.inputPair(1, gridLinearSize);
            int coordX = gridLinearSize - pair[1], coordY = pair[0] - 1;
            if(checkValid(coordX, coordY)) {
                grid[coordX][coordY] = MARK_X;
                break;
            }
            else
                System.out.println("Cell isn't empty, please try again ...");
        }
    }

    private void doResponseTurn() {
        int[] coords = doTryToWin();
        if(coords == null)
            coords = doBlockTurn();
        if(coords == null)
            coords = doRandomTurn();
        grid[coords[0]][coords[1]] = MARK_O;
        System.out.format("\nOpponent's turn: %d %d\n", coords[1] + 1, gridLinearSize - coords[0]);
    }

    private int[] doBlockTurn() {
        for(int length = victoryCondition - 1; length >= 2; length--) {
            int[] tempPair = findLineAndContinueIt(MARK_X, length);
            if(tempPair != null)
                return tempPair;
        }
        return null;
    }

    private int[] doTryToWin() {
        return findLineAndContinueIt(MARK_O, victoryCondition - 1);
    }

    private int[] findLineAndContinueIt(char mark, int length) {
        int[][] lines = findLinesOfMarks(mark, length);
        if(lines == null)
            return null;
        int linesSize = lines.length;
        for(int i = 0; i != linesSize; i++) {
            int[] line = lines[i];
            if (line == null)
                return null;
            int[] direction = {(line[2] - line[0]) / (length - 1), (line[3] - line[1]) / (length - 1)};
            if (checkValid(line[0] - direction[0], line[1] - direction[1]))
                return (new int[]{line[0] - direction[0], line[1] - direction[1]});
            if (checkValid(line[2] + direction[0], line[3] + direction[1]))
                return (new int[]{line[2] + direction[0], line[3] + direction[1]});
        }
        return null;
    }

    private int[] doRandomTurn() {
        while(true) {
            int coordX = random.nextInt(gridLinearSize);
            int coordY = random.nextInt(gridLinearSize);
            if(checkValid(coordX, coordY))
                return (new int[]{coordX, coordY});
        }
    }

    private boolean checkValid(int coordX, int coordY) {
        if(coordX >= gridLinearSize || coordY >= gridLinearSize || coordX < 0 || coordY < 0)
            return false;
        return (grid[coordX][coordY] == EMPTY);
    }

    private boolean checkGameOver() {
        return checkVictory() || checkFullFilled();
    }

    private boolean checkVictory() {
        int[][] tempArray = findLinesOfMarks(MARK_X, victoryCondition);
        if(tempArray != null) {
            System.out.println("\nVictory!");
            return true;
        }
        tempArray = findLinesOfMarks(MARK_O, victoryCondition);
        if(tempArray != null) {
            System.out.println("\nDefeat!");
            return true;
        }
        return false;
    }

    private boolean checkFullFilled() {
        for (int i = 0; i != gridLinearSize; i++)
            for (int j = 0; j != gridLinearSize; j++)
                if(grid[i][j] == EMPTY)
                    return false;
        System.out.println("\nDraw!");
        return true;
    }

    private int[][] findLinesOfMarks(char mark, int length) {
        int maxPossibleSize = gridLinearSize * gridLinearSize * 4;
        int[][] tempArray = new int[maxPossibleSize][];
        int count = 0;
        final int directions[][] = {{0, 1}, {1, 0}, {1, 1}, {1, -1}};
        // (i, j) - start coords, direction[k] - direction for check
        for (int i = 0; i != gridLinearSize; i++)
            for (int j = 0; j != gridLinearSize; j++) {
                for(int k = 0; k != directions.length; k++)
                    if(i + (length - 1) * directions[k][0] < gridLinearSize && i + (length - 1) * directions[k][0] >= 0 &&
                            j + (length - 1) * directions[k][1] < gridLinearSize && j + (length - 1) * directions[k][1] >= 0) {
                        int delta;
                        for(delta = 0; delta < length; delta++)
                            if(grid[i + delta * directions[k][0]][j + delta * directions[k][1]] != mark)
                                break;
                        if(delta == length)
                            tempArray[count++] = (new int[]{i, j, i + (length - 1) * directions[k][0], j + (length - 1) * directions[k][1]});
                    }
            }
        if(count == 0)
            tempArray = null;
        return tempArray;
    }

}
