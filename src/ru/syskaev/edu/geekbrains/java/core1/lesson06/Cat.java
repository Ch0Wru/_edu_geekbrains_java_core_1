package ru.syskaev.edu.geekbrains.java.core1.lesson06;

/**
 *  Course: Java Core 1
 *  Homework for lesson 6
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 27, 2018
 */
public class Cat extends Animal{

    public Cat(String name, int maxRunDistance, double maxJumpHeight) {
        super(name, maxRunDistance, maxJumpHeight);
    }

    @Override
    public boolean swim(int distance) {
        System.out.println(name + " can't swim");
        return false;
    }

}
