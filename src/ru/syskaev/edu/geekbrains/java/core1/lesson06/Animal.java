package ru.syskaev.edu.geekbrains.java.core1.lesson06;

/**
 *  Course: Java Core 1
 *  Homework for lesson 6
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 27, 2018
 */
abstract public class Animal {

    protected String name;
    private int maxRunningDistance;
    private double maxJumpingHeight;

    protected Animal(String name, int maxRunningDistance, double maxJumpingHeight) {
        this.name = name;
        this.maxRunningDistance = maxRunningDistance;
        this.maxJumpingHeight = maxJumpingHeight;
    }

    public boolean run(int distance) {
        boolean result = distance <= maxRunningDistance;
        System.out.format("%s is running\tTarget: %d   \tResult: %s\n", name, distance, Boolean.toString(result));
        return result;
    }

    public boolean jump(double height) {
        boolean result = height <= maxJumpingHeight;
        System.out.format("%s is jumping\tTarget: %.2f\tResult: %s\n", name, height, Boolean.toString(result));
        return result;
    }

    abstract boolean swim(int distance);

}
