package ru.syskaev.edu.geekbrains.java.core1.lesson06;

/**
 *  Course: Java Core 1
 *  Homework for lesson 6
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 27, 2018
 */
public class Dog extends Animal{

    private int maxSwimmingDistance;

    public Dog(String name, int maxRunDistance, double maxJumpHeight, int maxSwimmingDistance) {
        super(name, maxRunDistance, maxJumpHeight);
        this.maxSwimmingDistance = maxSwimmingDistance;
    }

    @Override
    public boolean swim(int distance) {
        boolean result = distance <= maxSwimmingDistance;
        System.out.format("%s is swimming\tTarget: %d   \tResult: %s\n", name, distance, Boolean.toString(result));
        return result;
    }

}
