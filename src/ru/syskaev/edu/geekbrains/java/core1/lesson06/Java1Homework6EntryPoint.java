package ru.syskaev.edu.geekbrains.java.core1.lesson06;

/**
 *  Course: Java Core 1
 *  Homework for lesson 6
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 27, 2018
 */
public class Java1Homework6EntryPoint {

    public static void main(String args[]) {
        Animal[] animals = prepareTest();
        System.out.print("Test #1\n\n");
        doTest(animals, 200, 2.0, 10);
        System.out.print("\n\nTest #2\n\n");
        doTest(animals, 500, 0.5, 5);
    }

    private static Animal[] prepareTest() {
        Animal[] animals = new Animal[6];
        animals[0] = new Cat("Barsik", 150, 1.5);
        animals[1] = new Dog("Polkan", 400, 0.45, 8);
        animals[2] = new Cat("Vasik", 200, 2.0);
        animals[3] = new Dog("Muhtar", 500, 0.5, 10);
        animals[4] = new Cat("Masik", 250, 2.5);
        animals[5] = new Dog("Tuzik", 600, 0.55, 12);
        return animals;
    }

    private static void doTest(Animal[] animals, int runTarget, double jumpTarget, int swimTarget) {
        for(Animal animal : animals) {
            animal.run(runTarget);
            animal.jump(jumpTarget);
            animal.swim(swimTarget);
            System.out.println();
        }
    }

}
