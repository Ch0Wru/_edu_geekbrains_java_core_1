package ru.syskaev.edu.geekbrains.java.core1.lesson01;

/**
 *  Course: Java Core 1
 *  Homework for lesson 1
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 10, 2018
 */
public class Java1Homework1EntryPoint {

    //Consts for methods test
    private static final int TASK3_A = 1, TASK3_B = 2, TASK3_C = 3, TASK3_D = 4;
    private static final int TASK4_A = 10, TASK4_B = 15;
    private static final int TASK5_A = -7;
    private static final int TASK6_A = 34;
    private static final String TASK7_NAME = "Ivan";
    private static final int TASK8_YEAR = 1600;

    public static void main(String[] args) {
        doTest();
    }

    private static void doTest() {
        HomeworkMethods.task2Method();
        System.out.format("Test task3Method() for A=%d, B=%d, C=%d, D=%d\nResult: %f\n\n",
                TASK3_A, TASK3_B, TASK3_C, TASK3_D, HomeworkMethods.task3Method(TASK3_A, TASK3_B, TASK3_C, TASK3_D));
        System.out.format("Test task4Method() for A=%d, B=%d\nResult: %b\n\n",
                TASK4_A, TASK4_B, HomeworkMethods.task4Method(TASK4_A, TASK4_B));
        System.out.format("Test task5Method() for A=%d\n", TASK5_A);
        HomeworkMethods.task5Method(TASK5_A);
        System.out.format("Test task6Method() for A=%d\nResult: %s\n\n",
                TASK6_A, HomeworkMethods.task6Method(TASK6_A));
        System.out.format("Test task7Method() for Name=\"%s\"\n", TASK7_NAME);
        HomeworkMethods.task7Method(TASK7_NAME);
        System.out.format("Test task8Method() for Year=%d\n", TASK8_YEAR);
        HomeworkMethods.task8Method(TASK8_YEAR);

    }

}
