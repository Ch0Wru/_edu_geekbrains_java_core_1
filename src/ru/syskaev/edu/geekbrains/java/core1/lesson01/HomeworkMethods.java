package ru.syskaev.edu.geekbrains.java.core1.lesson01;

/**
 *  Course: Java Core 1
 *  Homework for lesson 1
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 10, 2018
 */
public class HomeworkMethods {

    public static void task2Method() {
        byte a = 7;
        short b = 77;
        int c = 777;
        long d = 7777;
        float e = 77777.0f;
        double f = 777777.0;
        boolean g = true;
        String h = "7777777";
    }

    public static double task3Method(int a, int b, int c, int d) {
        if(d == 0) {
            System.out.println("Error: division by zero");
            return -1;
        }
        else
            return a * (b + ((double)c / d));
    }

    public static boolean task4Method(int a, int b) { return (a + b) <= 20 && (a + b) >= 10; }

    public static void task5Method(int a) { System.out.println((a >= 0 ? "Positive" : "Negative") + '\n'); }

    public static boolean task6Method(int a) { return a < 0; }

    public static void task7Method(String name) { System.out.println("Привет, " + name + "!\n"); }

    public static void task8Method(int year) {
        boolean tempBool = false;
        if((year % 4 == 0) && (year % 100 != 0)) tempBool = true;
        if(year % 400 == 0) tempBool = true;
        System.out.println(tempBool ? "Leap year" : "Regular year");
    }

}
