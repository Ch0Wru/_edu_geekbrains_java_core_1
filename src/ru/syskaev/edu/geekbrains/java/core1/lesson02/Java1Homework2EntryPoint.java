package ru.syskaev.edu.geekbrains.java.core1.lesson02;

import static ru.syskaev.edu.geekbrains.java.core1.lesson02.Utils.*;

/**
 *  Course: Java Core 1
 *  Homework for lesson 2
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 13, 2018
 */
public class Java1Homework2EntryPoint {

    //Consts for methods test
    private static final int TASK1_MIN_ARRAY_SIZE = 5, TASK1_MAX_ARRAY_SIZE = 20;
    private static final int TASK2_DEFAULT_ARRAY_SIZE = 8;
    private static final int[] TASK3_DEFAULT_ARRAY = {1, 5, 3, 2, 11, 4, 5, 2, 4, 8, 9, 1};
    private static final int TASK4_MIN_ARRAY_SIZE = 3, TASK4_MAX_ARRAY_SIZE = 10;
    private static final int TASK5_MIN_ARRAY_SIZE = 10, TASK5_MAX_ARRAY_SIZE = 30;
    private static final int TASK5_MIN_POSSIBLE_VALUE_IN_CELL = -100, TASK5_MAX_POSSIBLE_VALUE_IN_CELL = 100;
    private static final int TASK6_MIN_ARRAY_SIZE = 2, TASK6_MAX_ARRAY_SIZE = 10;
    private static final int TASK6_MIN_POSSIBLE_VALUE_IN_CELL = 0, TASK6_MAX_POSSIBLE_VALUE_IN_CELL = 2;
    private static final int TASK7_DEFAULT_SIZE = 10;
    private static final int TASK7_MIN_POSSIBLE_VALUE_IN_CELL = 0, TASK7_MAX_POSSIBLE_VALUE_IN_CELL = 9;
    private static final int TASK7_MIN_SHIFT_SIZE = -1000, TASK7_MAX_SHIFT_SIZE = 1000;

    public static void main(String[] args) {
        doTest();
    }

    private static void doTest() {
        // 1
        System.out.println("Test task 1...");
        int arraySize = randomIntBetween(TASK1_MIN_ARRAY_SIZE, TASK1_MAX_ARRAY_SIZE);
        int[] array = getTestArrayForTask1(arraySize);
        System.out.println("Origin array: ");
        printLinearArray(array);
        System.out.println("Changed array: ");
        invertArray(array);
        printLinearArray(array);
        // 2
        System.out.println("\nTest task 2...");
        array = new int[TASK2_DEFAULT_ARRAY_SIZE];
        System.out.println("Array: ");
        fillArrayForSecondTask(array);
        printLinearArray(array);
        // 3
        System.out.println("\nTest task 3...");
        array = TASK3_DEFAULT_ARRAY.clone();
        System.out.println("Origin array: ");
        printLinearArray(array);
        System.out.println("Changed array: ");
        changeArrayForThirdTask(array);
        printLinearArray(array);
        // 4
        System.out.println("\nTest task 4...");
        int tdArraySize = randomIntBetween(TASK4_MIN_ARRAY_SIZE, TASK4_MAX_ARRAY_SIZE);
        int[][] tdArray = new int[tdArraySize][tdArraySize];
        System.out.println("Array: ");
        diagonalizeArray(tdArray);
        printTwoDimensionalArray(tdArray);
        // 5
        System.out.println("\nTest task 5...");
        arraySize = randomIntBetween(TASK5_MIN_ARRAY_SIZE, TASK5_MAX_ARRAY_SIZE);
        array = getTestArrayForTask5or6or7(arraySize, TASK5_MIN_POSSIBLE_VALUE_IN_CELL, TASK5_MAX_POSSIBLE_VALUE_IN_CELL);
        System.out.println("Array: ");
        printLinearArray(array);
        System.out.println("Min value = " + minValueAtArray(array) + "\nMax value = " + maxValueAtArray(array));
        // 6
        System.out.println("\nTest task 6...");
        arraySize = randomIntBetween(TASK6_MIN_ARRAY_SIZE, TASK6_MAX_ARRAY_SIZE);
        array = getTestArrayForTask5or6or7(arraySize, TASK6_MIN_POSSIBLE_VALUE_IN_CELL, TASK6_MAX_POSSIBLE_VALUE_IN_CELL);
        System.out.println("Array: ");
        printLinearArray(array);
        System.out.println("Check balance = " + checkBalance(array));
        // 7
        System.out.println("\nTest task 7...");
        array = getTestArrayForTask5or6or7(TASK7_DEFAULT_SIZE, TASK7_MIN_POSSIBLE_VALUE_IN_CELL, TASK7_MAX_POSSIBLE_VALUE_IN_CELL);
        System.out.println("Origin array: ");
        printLinearArray(array);
        int shiftSize = randomIntBetween(TASK7_MIN_SHIFT_SIZE, TASK7_MAX_SHIFT_SIZE);
        System.out.println("Size of cycle shift = " + shiftSize);
        System.out.println("Changed array: ");
        cycleShiftArray(array, shiftSize);
        printLinearArray(array);
    }

}
