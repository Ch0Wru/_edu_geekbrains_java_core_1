package ru.syskaev.edu.geekbrains.java.core1.lesson02;

import java.util.Random;

/**
 *  Course: Java Core 1
 *  Homework for lesson 2
 *  @author Syskaev Andrey / Ch0W
 *  @version dated Jul 13, 2018
 */
public class Utils {

    private static final int DEFAULT_RANDOM_INT = 15;

    public static int[] getTestArrayForTask1(int arraySize) {
        int[] array = new int[arraySize];
        for(int i = 0; i != arraySize; i++)
            array[i] = (randomBoolean() ? 1 : 0);
        return array;
    }

    public static int[] getTestArrayForTask5or6or7(int arraySize, int minValue, int maxValue) {
        int[] array = new int[arraySize];
        for(int i = 0; i != arraySize; i++)
            array[i] = randomIntBetween(minValue, maxValue);
        return array;
    }

    public static void invertArray(int[] array) {
        int size = array.length;
        for(int i = 0; i != size; i++)
            array[i] = Math.abs(array[i] - 1);
    }

    public static void fillArrayForSecondTask(int[] array) {
        int size = array.length;
        for(int i = 0; i != size; i++)
            array[i] = i * 3;
    }

    public static void changeArrayForThirdTask(int[] array) {
        int size = array.length;
        for(int i = 0; i != size; i++)
            if(array[i] < 6)
                array[i] *= 2;
    }

    public static void diagonalizeArray(int[][] tdArray) {
        int size = tdArray.length;
        for(int i = 0; i != size; i++)
            tdArray[i][i] = tdArray[i][size - i - 1] = 1;
    }

    public static int minValueAtArray(int[] array) {
        int min = Integer.MAX_VALUE;
        for(int value : array)
            if(min > value)
                min = value;
        return min;
    }

    public static int maxValueAtArray(int[] array) {
        int max = Integer.MIN_VALUE;
        for(int value : array)
            if(max < value)
                max = value;
        return max;
    }

    public static boolean checkBalance(int[] array) {
        int fullSum = sumOfElements(array), tempSum = 0;
        if(fullSum % 2 != 0) return false;
        int size = array.length;
        for(int i = 0; i < size - 1; i++) {
            tempSum += array[i];
            if(2 * tempSum == fullSum)
                return true;
        }
        return false;
    }

    public static int sumOfElements(int[] array) {
        int sum = 0;
        for(int value : array)
            sum += value;
        return sum;
    }

    public static void cycleShiftArray(int[] array, int shiftSize) {
        int size = array.length;
        if(shiftSize < 0)
            shiftSize += size * (-shiftSize / size + 1);
        else
            shiftSize -= size * (shiftSize / size);
        while(shiftSize-- != 0)
            cycleShiftArrayRightOnce(array);
    }

    public static void cycleShiftArrayRightOnce(int[] array) {
        int size = array.length, tempLastElement = array[size - 1];
        for(int i = size - 1; i > 0; i--)
            array[i] = array[i - 1];
        array[0] = tempLastElement;
    }

    public static boolean randomBoolean() {
        return (new Random()).nextBoolean();
    }

    public static int randomIntBetween(int startInt, int endInt) /*inclusive*/ {
        if(endInt < startInt)
            return DEFAULT_RANDOM_INT;
        return (new Random()).nextInt(endInt - startInt + 1) + startInt;
    }

    public static void printLinearArray(int[] array) {
        for(int value : array)
            System.out.format("%3d ", value);
        System.out.println();
    }

    public static void printTwoDimensionalArray(int[][] array) {
        for(int[] linearArray : array) {
            for(int value : linearArray)
                System.out.format("%d ", value);
            System.out.println();
        }
    }

}
