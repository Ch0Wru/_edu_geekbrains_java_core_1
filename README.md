Все права на все используемые изображения принадлежат их авторам или их владельцам. <br/>
Изображения используются в частном порядке в образовательном проекте. <br/>
Изображения не демонстрируются публично и не используются в каких-либо коммерческих целях. <br/>

All rights to all used images belong to their authors or their owners. <br/>
Images are used privately in the educational project. <br/>
Images are not shown publicly and are not used for any commercial purposes. <br/>